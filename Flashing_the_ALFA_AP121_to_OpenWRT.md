Flashing the ALFA AP121 to OpenWRT
======================================

Using resources:

* [Tutorial on OpenWRT wiki](https://wiki.openwrt.org/toh/alfa.network/hornet-ub)
* [Screen recording of flashing same hardware to a pineapply mark iv](https://www.youtube.com/watch?v=i58XOF4Rfc4&feature=youtu.be)
* [useful forum post](https://forums.hak5.org/index.php?/topic/29810-alfa-ap121u-login-and-password/) continaed the secret telnet login password "root/80546334" which actually you don't need...
* [Installing OpenWrt via TFTP](https://wiki.openwrt.org/doc/howto/generic.flashing.tftp?s[]=tftpd)
* http://chschneider.eu/linux/server/tftpd-hpa.shtml

Indeed the default IP address is 192.168.2.1.

So Lesson 1: you need to have the serial connection open WHEN you power up the router, then you go straight into the "uboot" console (otherwise you get a telnet? login where you need the numeric root password -- above)

tftp
-----
the server:

	apt-get install tftpd-hpa


```
# /etc/default/tftpd-hpa
TFTP_USERNAME="tftp"
TFTP_DIRECTORY="/srv/tftp"
TFTP_ADDRESS="0.0.0.0:69"
TFTP_OPTIONS="--secure"
```

CHECK IT:
	
	sudo service tftpd-hpa status


COPY the .bin file to /srv/tftp/

	sudo cp ~/Downloads/openwrt-ar71xx-generic-hornet-ub-squashfs-sysupgrade.bin /srv/tftp/


Connect USB Serial adapter hooked up to (powered down) router.
Open the connection:

	sudo picocom -b 115200 /dev/ttyUSB0


Power up device, and pick: (1) for u-Boot

setenv serverip 192.168.2.11
setenv ipaddr 192.168.2.1

the pull the file:

	# tftp 0xa0800000 openwrt-ar71xx-generic-hornet-ub-jffs2-sysupgrade.bin

actually I used the squashfs:

	tftp 0xa0800000 openwrt-ar71xx-generic-hornet-ub-squashfs-sysupgrade.bin

and note that my byte count is slightly different.

	erase 0x9f050000 +0x790000

	cp.b 0xa0800000 0x9f050000 0x790000 

	reset

(and allow default boot from flash)

and OpenWRT starts!!!



