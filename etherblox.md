
Etherbloxx
===========

SoC + Debian
----------

SoCBloK / RASPIAN / DEBIAN

(aka Raspberry Pi, Banana Pi, BeagleBone, OliMex, )
Inexpensive hardware like the Pi's real strength comes from it's use of Linux distrubutions like Raspian (a Pi-specific spin of the Debian distribution), and thus access to a universe of free software available via the *dpkg* package management system. So on the Micro SD card powering a Pi is a cross section of decades of experience, labour and heritage of UNIX and GNU+Linux.


OpenWRT
----------
Block varieties: TP-LINK 3020, Alfa AP121

While networking tasks could be handled by a SoCBlok like a Raspberry Pi, the hardware of a device built for (wireless) networking tends to be more performant and scalable to a larger number of users. Plus the OpenWRT ecosystem is itself an interesting one with a package manager (*OPKG*) giving access to a variety of tasks specific to routing and networking.


Switch/Hub
---------

A switch or hub acts as a node to join many device together on a wired network. (8022.11n 150MB/s, CAT5 = 1Gb/s) Wired networks offer a maxium speed that's typically 10 times that of a wireless connection and are more stable, operate with less power consumption and produce signficantly less electro-magnetic radiation.
