Connecting to TP-LINK 3020 in Recovery Mode to fix things
=========================================================
When the router is starting up, when the "WPS/Reset" button first lights up, press it and it should start blinking quickly. If so, congratulations, the device is now in openwrt's "failsafe" mode.

Connect the router to your laptop with an ethernet cable.

In order for your laptop to be able to connect to the router, you simply need to establish that they are on the same subnet of a network (since they are physically connected). This means adjusting the "Wired settings" of your laptop's network settings.

openwrt's failsafe mode assigns the device IP address 192.168.1.1

