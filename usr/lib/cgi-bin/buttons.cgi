#!/usr/bin/env python
import os

env = os.environ
uri = env.get("REQUEST_URI")

print "Content-type: text/html"
print

if uri == "/etherdump/":
    print """
<form method="post" action="/cgi-bin/etherdump.cgi">
<input type="submit" value="etherdump" />
<input type="hidden" name="src" value="{0[REQUEST_URI]}" />
</form>
""".format(os.environ)
