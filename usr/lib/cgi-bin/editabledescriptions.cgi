#!/usr/bin/env python
import cgi, os
import re, sys, json
import cgitb; cgitb.enable()
from urlparse import urlparse

# print "Content-type: text/html"
# print
# cgi.print_environ()
# import sys; sys.exit(0)

class htaccess (object):
    def __init__ (self, path):
        self.path = path
        self.lines = []
        self.descriptions = {}

    def parse (self):
        with open(self.path) as f:
            pat = re.compile("^AddDescription \s+ \"(?P<description>.+?)\" \s+ \"?(?P<path>.+?)\"? \s*$", flags=re.X)
            self.lines = []
            self.descriptions = {}
            for line in f:
                m = pat.match(line)
                if m:
                    d = m.groupdict()
                    self.descriptions[d['path']] = self.unescape_d(d['description'])
                else:
                    self.lines.append(line)
        return self

    def unescape_d (self, n):
        return re.sub("\\\"", "\"", n)

    def escape_d (self, n):
        # slash escape non-escaped string chars
        return re.sub("(?<!\\\\)\"", "\\\"", n)

    def unparse (self, f):
        for line in self.lines:
            f.write(line)
        # f.write("\n")
        for filename, description in sorted(self.descriptions.items()):
            f.write("""AddDescription \"{0}\" \"{1}\"\n""".format(self.escape_d(description), filename))
        return self

    def set (self, filename, description):
        self.descriptions[filename] = description

    def save (self):
        try:
            os.rename(self.path, self.path+"~")
        except Exception:
            pass

        with open(self.path, "w") as f:
            self.unparse(f)

# ht = htaccess("/var/www/.htaccess")
# ht.parse()
# ht.set("tomato", 'hello \"world\"')
# ht.unparse(sys.stdout)
# sys.exit(0)

request_method = os.environ.get("REQUEST_METHOD", "GET").upper()
request_uri = os.environ.get("REQUEST_URI", "")
http_referer = os.environ.get("HTTP_REFERER", "")

if request_method == "POST":
    fs = cgi.FieldStorage()
    # with open("/usr/lib/cgi-bin/editabledescriptions.log", "a") as f:
    ref = urlparse(fs.getvalue("_referer", "")).path.lstrip("/")
    htpath = os.path.join(os.environ.get("DOCUMENT_ROOT"), ref, ".htaccess")
    #    f.write("ref: {0}, htpath: {1}\n".format(ref, htpath))
    response = {}
    response['path'] = htpath

    ht = htaccess(htpath)
    try:
        ht.parse()
    except IOError, OSError :
        pass
    for key in fs.keys():
        if key.startswith("_"):
            continue
        ht.set(key.rstrip("/"), fs.getvalue(key))
    ht.save()

    print "Content-type: application/json"
    print
    print json.dumps(response)
    sys.exit(0)

print "Content-type: text/javascript"
print
print """
(function () {
    var editing = false,
        request_uri = \"""" + request_uri + """\",
        http_referer = \"""" + http_referer + """\";

    function formencode (data) {
        var ret = "";
        for (var key in data) {
            if (ret) { ret += "&" }
            ret += (key + "=" + encodeURIComponent(data[key]));
        }
        return ret;
    }

    function do_save () {
        var data = { '_referer': http_referer };
        var names = document.querySelectorAll("td:nth-child(2) a"),
            descs = document.querySelectorAll("td:nth-child(5)");
        for (var i=0, l=names.length; i<l; i++) {
            var name = names[i].innerHTML,
                desc = descs[i].innerHTML;
            desc = desc.replace(/(&nbsp;)*/g, "");
            console.log("name", name, desc);
            data[name] = desc;
        }
        console.log("save", request_uri, http_referer, data);
        var request = new XMLHttpRequest();
        request.open('POST', request_uri, true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status==200) {
                // console.log("success", request);
                window.location.reload();
            }
        }
        request.send(formencode(data));
    }
    function start () {
        var th = document.querySelector("th:nth-child(5)"),
            save = document.createElement("button"),
            cancel = document.createElement("button");
        save.innerHTML = "save";
        th.appendChild(save);
        save.addEventListener("click", do_save);
        save.style.background = "pink";
        cancel.innerHTML = "cancel";
        cancel.style.background = "pink";
        th.appendChild(cancel);
        cancel.addEventListener("click", function () {
            window.location.reload();
        });
    }
    function editable () {
        var elt = this;
        this.setAttribute("contenteditable", true);
        this.addEventListener("keydown", function () {
            console.log("typing");
            if (!editing) { editing = true; start() };
            elt.style.background = "pink";
        })
    }
    window.addEventListener("load", function () {
        var ds = document.querySelectorAll("td:nth-child(5)");
        for (var i=0, l=ds.length; i<l; i++) {
            editable.call(ds[i]);
        }
    })

})()
"""