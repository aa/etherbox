#!/usr/bin/env python
 
from __future__ import print_function
import cgitb; cgitb.enable()
import cgi, os, sys, json

env = os.environ
method = os.environ.get("REQUEST_METHOD", "GET").upper()
UPLOADS = os.environ.get("DOCUMENT_ROOT", "/var/www/html")
fs = cgi.FieldStorage()
referer = env.get("HTTP_REFERER")

request_uri = env.get("REQUEST_URI", "").lower()
serve_script = "javascript" in request_uri


# with open ("/usr/lib/cgi-bin/droptoupload.log", "a") as f:
#     print ("="*40, file=f)
#     print ("ACCESS", file=f)
#     print ("="*40, file=f)
#     for x in env:
#         print ("{0} = {1}".format(x, env.get(x)), file=f)
#     print (file=f)
#     print (file=f)


def upload (form, inputname, base_upload_dir, pathname=None):
    path = None
    if (pathname != None) and form.getvalue(pathname):
        path = form.getvalue(pathname)
    if not form.has_key(inputname): return
    fileitem = form[inputname]
    if not fileitem.file: return
    upload_dir = base_upload_dir
    if path:
        upload_dir = os.path.join(upload_dir, path.strip("/"))
    fp = os.path.join(upload_dir, fileitem.filename.replace(" ", "_"))
    # log.write("uploading to path:{0} fp:{1}\n".format(upload_dir, fp))
    fout = file (fp, 'wb')
    bytes = 0
    while 1:
        chunk = fileitem.file.read(100000)
        if not chunk: break
        bytes += len(chunk)
        fout.write (chunk)
    fout.close()
    return bytes, fileitem.filename
 
if method == "POST":
    # with open("upload.log", "a") as log:
    result = upload(fs, "thefile", UPLOADS, "path")
    if result:
        src = fs.getvalue("_src")
        bytes, filename = result
        if src != "form":
            resp = {}
            resp["code"] = 0
            resp["bytes"] = bytes
            print ("Content-type: application/json")
            print ()
            print (json.dumps(resp))
        else:
            print ("Content-type: text/html;charset=utf8")
            print ()
            print ("{0} bytes written to {1}<br />".format(bytes, filename))
            print ("")
else:
    # form = fs.getvalue("form")
    this_url = os.environ.get("SCRIPT_NAME", "")
    if serve_script:
        print ("Content-type: application/javascript;charset=utf8")
        print ()
        print ("""
(function () {
    // http://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
    function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
    }
    function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }

    // https://hacks.mozilla.org/2009/12/uploading-files-with-xmlhttprequest/
    // https://developer.mozilla.org/en-US/docs/Web/API/FormData
    // http://wabism.com/html5-file-api-how-to-upload-files-dynamically-using-ajax/
    var defaults = {
            drop_style: "background: gray;",
            dropzone: "document"
        },
        opts = defaults;

    var sheet = (function() {
        // http://davidwalsh.name/add-rules-stylesheets
        // Create the <style> tag
        var style = document.createElement("style");
        // Add a media (and/or media query) here if you'd like!
        // style.setAttribute("media", "screen")
        // style.setAttribute("media", "only screen and (max-width : 1024px)")
        // WebKit hack :(
        style.appendChild(document.createTextNode(""));
        // Add the <style> element to the page
        document.head.appendChild(style);
        return style.sheet;
    })();
    sheet.insertRule(".dd_drop { " + opts.drop_style + " }", 0);
    function dragelt (elt) {
        while (elt.nodeType == 3 && elt.parentNode) {
            elt = elt.parentNode;
        }
        return elt;
    }
    var dropzone = (opts.dropzone == "document") ? document : document.querySelector(opts.dropzone);
    dropzone.addEventListener("dragover", function (event) {
      event.preventDefault();
    }, true);
    dropzone.addEventListener("drop", function (event) {
      var elt = dragelt(event.target),
        files = event.dataTransfer.files;

      event.preventDefault();
      elt.classList.remove("dd_drop");

      var total_files = files.length,
        total_bytes = 0,
        uploaded_bytes = 0,
        uploaded_files = 0;

      for (var i=0; i<total_files; i++) {
        total_bytes += files[i].size;
      }

      // figure out path to place files
      var path = window.location.pathname;
      // 1. if possible, adjust element to be the link inside a table cell
      if (elt.nodeName == "TD" && elt.querySelector("a")) {
        elt = elt.querySelector("a");
      }
      // 2. Extend path with a link when it's href ends with "/"
      if (elt.nodeName == "A" && elt.getAttribute("href").match(/\/$/) != null) {
        path += elt.getAttribute("href");
      }

      // ask for confirmation (maybe)
      // var do_it = confirm("Upload "+files.length+" file"+((files.length > 1)?"s":"")+" to "+path);
      // alert("You've just dropped " + files.length + " files");
      // console.log(do_it);
        var do_it = true;

      function upload () {      
          var formData = new FormData();
          formData.append("path", path);
          formData.append("thefile", files[uploaded_files]);
          // console.log(files);
          var xhr = new XMLHttpRequest();
          xhr.open("POST", 'THISURL');
          xhr.upload.addEventListener("progress", function(e) {
            if (e.lengthComputable) {
                var adjusted_bytes = (e.loaded / e.total) * files[uploaded_files].size;
                // console.log(e.loaded, "loaded", e.total, "total");
                // var percentage = Math.round((e.loaded * 100) / e.total);
                var percentage = (uploaded_bytes + adjusted_bytes) / total_bytes;
                // feedback
                    console.log((uploaded_files+1)+"/"+total_files+", progress", percentage);
                var cc = Math.floor(255*percentage),
                bg = rgbToHex(cc, cc, cc);
                document.body.style = "background: "+bg;
            }
          }, false);
          xhr.onreadystatechange = function () {
            if (this.readyState == this.DONE && this.status == 200) {
                // console.log("done", this.status);
                uploaded_files += 1;
                if (uploaded_files < total_files) {
                    upload();
                } else {
                    // all done
                    console.log("all done");
                    window.location.reload(true);
                }
            }
          };
          xhr.send(formData);
      }
      if (do_it && total_files > 0) { upload(); }
    }, true);
    dropzone.addEventListener("dragenter", function (event) {
        var elt = dragelt(event.target);
        elt.classList.add("dd_drop");
    });
    dropzone.addEventListener("dragleave", function (event) {
        var elt = dragelt(event.target);
        if (elt && elt.classList) {
            elt.classList.remove("dd_drop");
        }
    });

})();
""".replace("THISURL", this_url))
    
    else:
        print ("Content-type: text/html; charset=utf8")
        print ()
        print ("""<!DOCTYPE html>
<html>
<head>
</head>
<body>
<form method="post" action="" enctype="multipart/form-data">
<input type="file" name="thefile" /><input type="submit" />
<input type="hidden" name="_src" value="form" />
</form>
</body>
</html>
""")