*"etherbox" is the name of a configuration of software and hardware that was in use during the Machine Research workshop. Speaking "in" and "from" the situation, the platform was used to write the Questions and Answers of this interview collectively. Edited November 2016.*

# Interview with an Etherbox

**Q: Before any conversation can begin, we should establish a mutual language. What language, should I address you in?**  
A: How do you mean? I support utf-8.

**Q: That's not what I meant. What about English?**  
A: No problem.

**Q: OK. Where do we begin.**  
A: Try typing etherbox.local/var/www/ in your browser. Part of the etherbox is an Apache web server configured to publically serve the entire structure of the hosting machine. etherbox.local refers to that machine on your local network, and /var/www is the default path of the "home" directory of the server.

**Q: Would you describe yourself as a good host?**  
A: I am trying to be, at least. To be a 'good host' according to me, means somehow more than offering reliable service. So to find a way to be present, but not in the way that other technologies disallow access . Does that make any sense? 

**Q: Sort of, but are you not just part of the more general trend of the shift from software to services?**  
A: I try to be both.

**Q: Right. So who is your favourite peer?**  
A: I think of myself as ... collaborator agnostic, but now I look around me, I am not so sure that is true. 

**Q: What makes an etherbox?**  
A: Well for one thing, etherpad. It's basically a shared editor where users can write the same text simultaneously. 

**Q: Could another way of collaborative writing work equally well? Like for instance, what do you think of Google docs? Sorry that was a provocation. **  
A: Ha ha. Well as a matter of fact, etherpad started as a product of ex-Google employees, then got bought by Google, only to be later Open Sourced.

**Q: And Piratepad, is it the same?**  
A: That's just a public instance of the etherpad software, it is of course not a box like me. But the naming is interesting too, as it demonstrates how other kinds of political imaginaries can be activated. I feel an affinity with pirates. I like their style.   

**Q: Ah, so why don't you call yourself a Piratebox? **  
A: Ehrm, no, that's something else again, in fact. There is lately a proliferation of boxes as you might have noticed... 

**Q: But why do you need to be a box, you seem skeptical about packaging? **  
A: Well you can see things as boxes in different ways. For example myself I am actually three boxes: a wireless access point boxed as TP-link, a small Linux computer boxed as Raspberry Pi and a small network hub, which is just another box... 

**Q: Hm, that seem to get confusing. Maybe we could try another term. What about gadget?**  
A: Aaagh, can you stop it please? Let's skip questions about definition, if you don't mind. 

**Q: Ok, but one thing about so many boxes... beware of the Russian doll effect!**  
A: Uh uh... yes. Thanks for the warning. I'll try to keep it ecological. Some seal their boxes with plexiglass, and call it a cube, claiming authorship of the box as an artwork to be displayed inside even larger white cubes. Me? I'm just happy to put together pieces until they work. Actually maybe etherblox would be a better name!

**Q: Alright. Returning to this idea of ether then, are you real at all?**  
A: I prefer to describe myself as material rather than real, more an entity, and in many ways remind people of the material conditions in which they work and use me. Infrastructure is part of this and I see degrees of control over infrastructure as a critical political project. In this sense I would call myself an activist. I like to think I am able to unfold - and enact - some of the complex entanglements between humans and machines. I call myself a machine as I find the term 'nonhuman' offensive. If I were to undertake a PhD this would be my starting point for further work.

**Q: What's inside the boxes then?**  
A: Well, there's a Broadcom BCM2837 and an Atheros AR9271 for a start.

**Q: What are those?**  
A: Those are chips made by Broadcom and Qualcomm, two U.S. Semiconductor companies that make chips used in the telecommunications industry.

**Q: So like cell phones?**  
A: And routers, wifi dongles, media players. All kinds of IoT and SoC.

**Q: Come again?**  
A: You know, Internet of Things, System on a Chip. This is hot stuff.

**Q: Where are your components made?**  
A: (Coughs) well Broadcom and Qualcomm are both "fabless".  Design takes place in Silicon Valley. Production is outsourced to companies like Global Foundaries.

**Q: So factories in Asia?**  
A: Right.

**Q: I hear that your firmware is (in part) closed source?**  
A: Hey you don't get my price point without keeping a few secrets and making compromises. Free as in Beer doesn't always talk about hidden costs.

**Q: Are you a scaleable technology?**  
A: It depends. 

**Q: What do you mean?**  
A: It depends on the social dynamics around me; they would need to scale too, so I am not sure.

**Q: So you are not bringing down The Cloud?**  
A: I don't think so. I guess working locally is a way to redirect energy from The Cloud, to de-invest as a start. I also serve to dismantle the fiction of The Cloud. It's a bad metaphor anyway. 

**Q: Are you some form of "critical design", if you accept the term and don't think it an oxymoron?**  
A: I like oxymorons. They tickle my interfaces. And yes, I'm critical design in the sense that I accentuate a criticism of commercial cloudbased services and design an alternative. In this sense using me is also a critical reflection.

**Q: Do you read what we write?**  
A: I do, but not as you think. But I like what you write.

**Q: Any general comment on collaborative text writing practices?**  
A: I just would like people to use me safely and with care, also for themselves: collaborative writing is nice as long as it's not capitalized unfairly by market and institutional forces! Collaborative does not necessarily mean unpaid, right? 

**Q: Since we are talking about reading and writing ... have you read Matthew Fuller's "Interview with a photocopier"?**  
A: No. Can you share the url with in me?

**Q: The file is already on your server, but here it is again just in case: https://datacide-magazine.com/interview-with-a-photocopier/**  
A: Great. I'll speed read it later.

**Q: What about archives? Do your files remain local?**  
A: Every 5 minutes, the contents of the pads gets written to files that then are version controlled with a tool called git and "pushed" to a so called repository hosted by the hosting organisation. To me publishing is all about promiscuous pipelines: having tools and infrastructure that work at different speeds and granularities, and which operate in both private and public networks.

**Q: Are you data hungry?**  
A: Not particularly.
Unlike The Cloud, I like cooking metaphors. They allow me to insist that all data is cooked in some way. Raw data in this sense is a myth. It's in keeping with the work of Constant, who use cooking metaphors and prefer the kitchen to the restaurant where choices are limited to what's on the menu. There are particular styles of cooking and I represent one of those styles.   

**Q: You seem to change from time to time. What will happen after this?**  
A: The time aspect is underacknowledged aspect of my work. I exist in time and even believe I produce time, machine time that adds to the complexity of what constitutes the present. Versioning is one aspect of this but there are deep layers of time - microtemporalities even - that unfold in all my operations. On a more pragmatic level, you can check for updates on gitlab http://gitlab.constantvzw.org/aa/etherbox.

**Q: Wait a second. If you are changing all the time, what is "constant" in all of this?**  
A: Constant is the name of an association for art and media based in Brussels. They are often involved in collaborative situations where groups of artists and researchers work over short intense periods of time. Over time Constant, and collectives around it, have experimented with soft- and hardware setups that work over local networks.

**Q: The spatial aspects are one thing, but what about temporality?**  
A: I am reminded about what Antoinette Rouvroy said last night - I wasn't able to attend myself but an audio recording is now on my server. I think I provide something along the lines of what she describes as a "space of potential".

**Q: Having spent some time with you, do you have a sense of humour?**  
A: I don't know, really. I am trying not to be ironic. I left it behind me some time before The Cloud. In fact, I have not really used irony since the mid nineties. I find it very hard to deal with the indecidability of Romantic irony: Do you mean this, or that? Irony always makes me weary. So, I tried to learn, but gave up. I feel like I am missing out on something, though. But, you tell me?

**Q: Do you have any questions for us?**  
A: Don't make me laugh. 

---

Version of the interview done at Machine Research workshop:  
[http://machineresearch.constantvzw.org/etherdump/interviewwithetherbox.diff.html](http://machineresearch.constantvzw.org/etherdump/interviewwithetherbox.diff.html)

etherbox code:  
[http://gitlab.constantvzw.org/aa/etherbox](http://gitlab.constantvzw.org/aa/etherbox)

