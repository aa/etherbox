# find all .md files in the directory
img=$(shell ls *.jpg)
derimg=$(shell ls *.small.jpg)
srcimg=$(filter-out $(derimg),$(img))

imgsmall=$(srcimg:%.jpg=%.small.jpg)
mdsrc=$(shell ls *.md)
# map *.mp => *.html for mdsrc
html_from_md=$(mdsrc:%.md=%.html)

all: $(html_from_md) $(imgsmall)

%.small.jpg: %.jpg
	convert -resize 240x $< $@

# Implicit rule to know how to make .html from .md
%.html: %.md template.html
	pandoc --from markdown \
		--to html \
		--standalone \
		--template template.html \
		--css styles.css \
		$< -o $@

# special rule for debugging variables
print-%:
	@echo '$*=$($*)'
